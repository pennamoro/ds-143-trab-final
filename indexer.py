import os
import re
import argparse
import mmap
from collections import defaultdict
from math import log10


def tokenize(texto):
    """
    Transforma as palavras em "tokens", ou seja, separar as palavras e deixando em letra minúscula.
    """
    return re.findall(r"\b\w+(?:[-']\w+)*\b", texto.lower(), re.UNICODE)


def conta_palavras(arq_caminho):
    """
    Conta o numero de occorrencias de cada palavra em um arquivo usando mapeamento de memória.
    """
    contagem_palavras = defaultdict(int)
    with open(arq_caminho, "r", encoding="latin-1") as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as mm:
            for linha in iter(mm.readline, b""):
                for palavra in tokenize(linha.decode("latin-1")):
                    contagem_palavras[palavra] += 1
    return contagem_palavras


def calcula_tfidf(palavra, arq_caminho, contagem_palavras, contagem_documentos, arq_caminhos):
    """
    Calcula a pontuação TF-IDF da palavra especificada no arquivo especificado, considerando as contagens de palavras e documentos, para todos os documentos.
    """
    tf = contagem_palavras[arq_caminho][palavra] / \
        sum(contagem_palavras[arq_caminho].values())
    idf_denominador = contagem_documentos[palavra]
    if idf_denominador == 0:
        return 0
    idf = log10(len(arq_caminhos) / idf_denominador)
    return tf * idf


def get_palavras_mais_frequentes(arq_caminho, n):
    """
    Retorna uma lista das n palavras mais frequentes no arquivo especificado, em ordem decrescente de contagem de ocorrências.
    """
    contagem_palavras = conta_palavras(arq_caminho)
    return sorted(contagem_palavras.items(), key=lambda x: x[1], reverse=True)[:n]


def get_contagem_palavras(arq_caminho, palavra):
    """
    Retorna o número de ocorrências da palavra no arquivo especificado.
    """
    contagem_palavras = conta_palavras(arq_caminho)
    return contagem_palavras[palavra]


def get_arquivos_mais_relevantes(palavra, arq_caminhos):
    """
    Retorna uma lista contendo o caminho do arquivo, sua pontuação TF-IDF correspondente para o termo de pesquisa especificado,
    e um valor booleano indicando se o termo de busca foi encontrado no arquivo, em ordem decrescente de relevância.
    """
    contagem_palavras = {}
    contagem_documentos = defaultdict(int)
    for arq_caminho in arq_caminhos:
        contagem = conta_palavras(arq_caminho)
        contagem_documentos[palavra] += 1 if palavra in contagem.keys() else 0
        contagem_palavras[arq_caminho] = contagem

    tfidf_pontos = []
    for arq_caminho in arq_caminhos:
        contagem = contagem_palavras[arq_caminho]
        pontuacao_tfidf = calcula_tfidf(
            palavra, arq_caminho, contagem_palavras, contagem_documentos, arq_caminhos)
        tfidf_pontos.append(
            (arq_caminho, pontuacao_tfidf, palavra in contagem))

    tfidf_pontos = sorted(tfidf_pontos, key=lambda x: x[1], reverse=True)
    return tfidf_pontos


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Realiza uma contagem de palavras nos arquivos")
    parser.add_argument("arq_caminhos", type=str, nargs="+",
                        help="Caminhos para os arquivos a serem procurados")
    parser.add_argument("--freq", type=int, metavar="N",
                        help="Mostra as N palavras mais frequentes no arquivo.")
    parser.add_argument("--freq-word", type=str, metavar="palavra",
                        help="Mostra a contagem de uma palavra no arquivo.")
    parser.add_argument("--search", type=str, metavar="termo", nargs="+",
                        help="Mostra os documentos mais relevantes para uma palavra.")
    args = parser.parse_args()

    # Coleta o caminho dos arquivos
    arq_caminhos = []
    for caminho in args.arq_caminhos:
        if os.path.isfile(caminho):
            arq_caminhos.append(caminho)
        elif os.path.isdir(caminho):
            for dir_caminho, _, nome_arqs in os.walk(caminho):
                for nome_arq in nome_arqs:
                    if nome_arq.endswith(".txt"):
                        arq_caminhos.append(
                            os.path.join(dir_caminho, nome_arq))

    # Realiza as ações
    if args.freq:
        for palavra, contagem in get_palavras_mais_frequentes(arq_caminhos[0], args.freq):
            print(f"{palavra}: {contagem}")
    elif args.freq_word:
        contagem = get_contagem_palavras(args.arq_caminhos[0], args.freq_word)
        print(f"{args.freq_word}: {contagem}")
    elif args.search:
        for termo in args.search:
            print(f"Resultados para o termo '{termo}':")
            arquivos_relevantes = get_arquivos_mais_relevantes(
                termo, arq_caminhos)
            if not arquivos_relevantes:
                print("Resultados não encontrados")
            else:
                for arq_caminho, pontuacao_tfidf, termo_encontrado in arquivos_relevantes:
                    if termo_encontrado:
                        print(f"{arq_caminho}: {pontuacao_tfidf:.7f}")
                    else:
                        print(f"{arq_caminho}: Termo não encontrado")
    else:
        parser.print_help()
