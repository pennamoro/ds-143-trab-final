<br >

# Trabalho Final : <span style="color:gold">Estrutura de Dados II</span>

## Descrição do programa:

Indexer é um programa que pode ser usado para procurar arquivos contendo palavras específicas. 

Ele é executado a partir da linha de comando e pode executar três ações, conforme especificado pelos argumentos da linha de comando:
    
    --freq: exibe as N palavras que ocorrem com mais frequência no arquivo especificado.

    --freq-word: exibe o número de ocorrências da palavra especificada no arquivo especificado.

    --search: exibe uma lista de arquivos que são mais relevantes para o termo de pesquisa especificado, com base no algoritmo TF-IDF (Term Frequency-Inverse Document Frequency).
### Outras especifícações:

- Usa o módulo **mmap** para executar um mapeamento de memória rápido e eficiente dos arquivos.
- Usa a classe **defaultdict** de Collections para criar um dicionário cujo padrão é zero para quaisquer chaves ausentes.

  
<br >

## Como utilizar :
- Tenha certeza que o Python está instalado
- Faça o clone do projeto na sua máquina
- Tenha certeza de colocar o caminho correto para os arquivos, ou deixe eles na mesma pasta do programa.

<br >

## Para <span style="color:lightseagreen">Python3</span> :
- <pre> python3 indexer.py [<span style="color:lightseagreen">ARQUIVO.txt</span>] --freq [<span style="color:lightseagreen">N</span>] </pre>
- <pre> python3 indexer.py [<span style="color:lightseagreen">ARQUIVO.txt</span>] --freq-word [<span style="color:lightseagreen">PALAVRA</span>] </pre>
- <pre> python3 indexer.py [<span style="color:lightseagreen">ARQUIVO1.txt ... ARQUIVON.txt</span>] --search [<span style="color:lightseagreen">PALAVRA</span>] </pre>
<br >

## Outras versões do <span style="color:mediumorchid">python</span> : 
- <pre> python indexer.py [<span style="color:mediumorchid">ARQUIVO.txt</span>] --freq [<span style="color:mediumorchid">N</span>] </pre>
- <pre> python indexer.py [<span style="color:mediumorchid">ARQUIVO.txt</span>] --freq-word [<span style="color:mediumorchid">PALAVRA</span>] </pre>
- <pre> python indexer.py [<span style="color:mediumorchid">ARQUIVO1.txt ... ARQUIVON.txt</span>] --search [<span style="color:mediumorchid">PALAVRA</span>] </pre>

<br >

# Integrantes :

<table>
    <tr>
        <th>Nome</th>
        <th>GRR</th>
    </tr>
    <tr>
        <td>Geovanna Alberti Correia de Freitas</td>
        <td><b>GRR20210548</b></td>
    </tr>
    <tr>
        <td>Guilherme Penna Moro</td>
        <td><b>GRR20211633</b></td>
    </tr>
    <tr>
        <td>Pietro Borges Parri</td>
        <td><b>GRR20204445</b></td>
    </tr>
</table>